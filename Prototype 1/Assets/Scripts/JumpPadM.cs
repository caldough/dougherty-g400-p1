using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPadM : MonoBehaviour
{
    float bounce = 20f;
    public AudioSource audioSource;

    void OnCollisionEnter2D(Collision2D collision)
    {



        if (collision.gameObject.CompareTag("Player"))
        {
            audioSource.Play();
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * bounce, ForceMode2D.Impulse);
        }
    }

}
