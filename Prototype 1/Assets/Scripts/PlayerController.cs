using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D rB2D;
    public float runSpeed; //ground move speed
    public float jumpSpeed = 1; //ground jump speed
    bool shouldJump;

    // Start is called before the first frame update
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
    }
     void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");


        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if (shouldJump == true)
        {
            rB2D.velocity = new Vector2(rB2D.velocity.x, rB2D.velocity.y + jumpSpeed);
            shouldJump = false;
        } 
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Jump")) // allows player to also change setting to assign a different button for jump
        {
            int levelMask = LayerMask.GetMask("Level"); //checks if ball is on ground

            if (Physics2D.CircleCast(transform.position, 0.1f, Vector2.down, 0.5f, levelMask))    
            {
                shouldJump = true;
            }
        }

    }
}
