using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RJumpPad : MonoBehaviour
{
    float bounce = 20f;


    void OnCollisionEnter2D(Collision2D collision)
    {


        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.down * bounce, ForceMode2D.Impulse);


        }
    }
}
